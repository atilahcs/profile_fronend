const jwt_secret = '';
var jwt = require('jsonwebtoken');
module.exports = async function(req, res, next) {
    console.log('ip:', req.ip);
    console.log('cookies:', req.cookies);
    console.log('policy: nonauthen');
    console.log('path:', req.path);
    console.log('method:', req.method);
    console.log('========================');
    var profile_id = req.cookies.profile;
    if (req.cookies.profile_token && profile_id) {
        try {
            const verfiy_result = await jwt.verify(req.cookies.profile_token, jwt_secret);
            console.log('verfify_result:', verfiy_result);
            if (req.xhr)
                return res.json(401, { message: 'You\'ve already loggedin!' });
            else
                return res.redirect('/profile');
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            return next();
        }
    } else {
        res.clearCookie('profile');
        res.clearCookie('profile_token');
        return next();
    }
}