const jwt_secret = '';
var jwt = require('jsonwebtoken');
module.exports = async function(req, res, next) {
    console.log('ip:', req.ip);
    console.log('cookies:', req.cookies);
    console.log('policy: authen');
    console.log('path:', req.path);
    console.log('method:', req.method);
    console.log('========================');
    var profile_id = req.cookies.profile;
    if (req.cookies.profile_token && profile_id) {
        try {
            const verfiy_result = await jwt.verify(req.cookies.profile_token, jwt_secret);
            if (verfiy_result.profile === Number(profile_id))
                return next();
            else {
                if (req.xhr)
                    return res.json(401, { message: 'Unauthorized access!' });
                else
                    return res.redirect('/login');
            }
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            console.error('============================');
            console.error('Policy: authen');
            console.error('Path: '+req.path);
            console.error('Error message: ', err.message);
            console.error('============================');
            if (req.xhr)
                return res.json(401, { message: 'Unauthorized access!' });
            else
                return res.redirect('/login');
        }
    } else if (!profile_id) {
        res.clearCookie('profile');
        res.clearCookie('profile_token');
        if (req.xhr)
            return res.json(401, { message: 'Session over! Please login again' });
        else
            return res.redirect('/login');
    } else {
        res.clearCookie('profile');
        res.clearCookie('profile_token');
        if (req.xhr)
            return res.json(401, { message: 'Unauthorized access!' });
        else
            return res.redirect('/login');
    }
}