const jwt_secret = '';
const FORGET_SECRET = '';
let jwt = require('jsonwebtoken');
let nodemailer = require('nodemailer');
let SENDER = '';
let USER = '';
let CLIENT_ID = '';
let CLIENT_SECRET = '';
let REFRESH_TOKEN = '';
let ACCESS_TOKEN = '';
module.exports = {
    //view
    loginView: (req, res)=>{
        return res.view('pages/login', {
            layout: 'layout/non_authen'
        });
    },
    registerView: (req, res)=>{
        return res.view('pages/register', {
            layout: 'layout/non_authen'
        });
    },
    forgetPasswordView: (req, res)=>{
        return res.view('pages/forget-password', {
            layout: 'layout/non_authen'
        });
    },
    resetPasswordView: async (req, res)=>{
        const { nkt } = req.allParams();
        try {
            const data = await jwt.verify(nkt, FORGET_SECRET);
            let email = data.email;
            return res.view('pages/reset-password', {
                layout: 'layout/non_authen',
                error: false,
                email: email,
                token: nkt
            });
        } catch (err) {
            return res.view('pages/reset-password', {
                layout: 'layout/non_authen',
                error: true
            });
        }
    },
    //action
    login: async (req, res)=>{
        const { email, password } = req.allParams();
        try {
            const profile_result = await HttpService.requestToProfile({ email: email, password: password },'/profile/authenticate', 'post');
            var token = jwt.sign({ profile: profile_result.profile.id }, jwt_secret, { expiresIn: 24 * 60 * 60 * 1000 });
            res.cookie('profile', profile_result.profile.id, { maxAge: (24 * 60 * 60 * 1000) }); //expire in millisecond, one day first to try
            res.cookie('profile_token', token, { maxAge: (24 * 60 * 60 * 1000) });
            return res.json({ profile: profile_result.profile });
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            console.error('============================');
            console.error('Controller: Authenticate');
            console.error('Action: Login');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    register: async (req, res)=>{
        const { firstname, lastname, email, password } = req.allParams();
        try {
            const profile_result = await HttpService.requestToProfile({ firstname: firstname, lastname: lastname, email: email, password: password },'/profile', 'post');
            var token = jwt.sign({ profile: profile_result.profile.id }, jwt_secret, { expiresIn: 24 * 60 * 60 * 1000 });
            res.cookie('profile', profile_result.profile.id, { maxAge: (24 * 60 * 60 * 1000) }); //expire in millisecond, one day first to try
            res.cookie('profile_token', token, { maxAge: (24 * 60 * 60 * 1000) });
            return res.json({ profile: profile_result.profile });
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            console.error('============================');
            console.error('Controller: Authenticate');
            console.error('Action: Register');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    forgetPassword: async(req, res)=>{
        const { email } = req.allParams();
        try {
            const forget_request = await HttpService.requestToForget({}, '/profile/'+email+'/forget-password/', 'get');
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    type: 'OAuth2',
                    user: USER,
                    clientId: CLIENT_ID,
                    clientSecret: CLIENT_SECRET,
                    refreshToken: REFRESH_TOKEN,
                    accessToken: ACCESS_TOKEN
                }
            });

            var token = await jwt.sign({ email: email }, FORGET_SECRET, { expiresIn: 30 * 60 * 1000 });
            let base_url = '';
            const email_content = await readFile(base_url, token, email);
            const email_result = await transporter.sendMail({ from: SENDER, to: email, cc: SENDER, subject: '[Wishbeer Profile] Forget your password', html: email_content });
            return res.json(true);
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            console.error('============================');
            console.error('Controller: Authenticate');
            console.error('Action: Forget Password');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(true);
        }
    },
    resetPassword: async (req, res)=>{
        const { email, new_pwd, confirm_pwd, token } = req.allParams();
        try {
            const data = await jwt.verify(token, FORGET_SECRET);
            if (email != data.email)
                throw { message: 'Invalid email/password' };
            if (new_pwd.length < 8)
                throw { message: 'Password cannot be less than 8 characters' };
            if (new_pwd !== confirm_pwd)
                throw { message: 'New password and confirm password are not the same' };

            const reset_request = await HttpService.requestToForget({ new_password: new_pwd, confirm_password: confirm_pwd },'/profile/'+email+'/forget-password/', 'put');
            return res.json(true);
        } catch (err) {
            res.clearCookie('profile');
            res.clearCookie('profile_token');
            console.error('============================');
            console.error('Controller: Authenticate');
            console.error('Action: Reset Password');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    logout: (req, res)=>{
        res.clearCookie('profile');
        res.clearCookie('profile_token');
        return res.redirect('/login');
    }
}
function readFile(base_url, token, email){
    return new Promise((resolve, reject)=>{
        let fs = require('fs');
        fs.readFile(sails.config.appPath+'/email_template/forget-password.html', 'utf8', function(err, email_content){
            if (err)
                reject({ message: err.message });
            else {
                email_content = email_content.replace('${base_url}', '');
                email_content = email_content.replace('${base_url}', '');
                email_content = email_content.replace('${base_url}', '');
                email_content = email_content.replace('${token}', token);
                email_content = email_content.replace('${token}', token);
                email_content = email_content.replace('${email}', email);
                resolve(email_content);
            }
        });
    });
}