module.exports = {
    //view
    profileView: async (req, res)=>{
        let profile_id = Number(req.cookies.profile);
        if (!profile_id || isNaN(profile_id)) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ProfileView');
            console.error('Error message: user not found');
            console.error('============================');
            return res.view('pages/error');
        }
        try {
            const profile_result =  await HttpService.requestToProfile({ }, '/profile/' + profile_id, 'get');
            return res.view('pages/account', {
                layout: 'layout/authen',
                profile: profile_result.profile,
                page_name: 'profile',
                tab: 'account',
                metadata: {
                    title: 'My Profile'
                }
            });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ProfileView');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.view('pages/error');
        }
    },
    changePasswordView: async (req, res)=>{
        let profile_id = Number(req.cookies.profile);
        if (!profile_id || isNaN(profile_id)) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ProfileView');
            console.error('Error message: user not found');
            console.error('============================');
            return res.view('pages/error');
        }
        try {
            const profile_result =  await HttpService.requestToProfile({ id: profile_id }, '/profile/' + profile_id, 'get');
            return res.view('pages/account', {
                layout: 'layout/authen',
                profile: profile_result.profile,
                page_name: 'change_password',
                tab: 'account',
                metadata: {
                    title: 'My Profile'
                }
            });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ChangePasswordView');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.view('pages/error');
        }
    },
    orderView: async (req, res)=>{
        let profile_id = Number(req.cookies.profile);
        if (!profile_id || isNaN(profile_id)) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ProfileView');
            console.error('Error message: user not found');
            console.error('============================');
            return res.view('pages/error');
        }
        try {
            const profile_result =  await HttpService.requestToProfile({ }, '/profile/' + profile_id, 'get');
            const order_list_result =  await HttpService.requestToProfile({ }, '/profile/' + profile_id+'/order', 'get');
            return res.view('pages/order_history',{
                layout: 'layout/authen',
                profile: profile_result.profile,
                order_list: order_list_result.order_list,
                page_name: 'order_list',
                tab: 'order',
                metadata: {
                    title: 'My Order'
                }
            });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: OrderView');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.view('pages/error');
        }
    },
    orderDetailView: async (req, res)=>{
        let profile_id = Number(req.cookies.profile)
        order_id = Number(req.param('order_id'));
        if (!profile_id || isNaN(profile_id)) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: ProfileView');
            console.error('Error message: user not found');
            console.error('============================');
            return res.view('pages/error');
        }
        try {
            const profile_result =  await HttpService.requestToProfile({ id: profile_id }, '/profile/' + profile_id, 'get');
            const order_detail_result =  await HttpService.requestToProfile({ id: profile_id }, '/profile/' + profile_id+'/order/'+order_id, 'get');
            console.log(order_detail_result);
            return res.view('pages/order_history',{
                layout: 'layout/authen',
                profile: profile_result.profile,
                order_detail: order_detail_result.order,
                page_name: 'order_detail',
                tab: 'order',
                metadata: {
                    title: 'My Order'
                }
            });
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: OrderDetailView');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.view('pages/error');
        }
    },
    //action
    update: async (req, res) => {
        const { id, email, firstname, lastname, phone, title } = req.allParams();
        try {
            const profile_result = await HttpService.requestToProfile({ email: email, firstname: firstname, lastname: lastname, phone: phone },'/profile/'+id, 'put');
            return res.json(profile_result);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: update');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
    updatePassword: async (req, res) => {
        const { id, current_password, new_password, confirm_password } = req.allParams();
        try {
            const profile_result = await HttpService.requestToProfile({ current_password: current_password, new_password: new_password, confirm_password: confirm_password },'/profile/'+id+'/password', 'put');
            return res.json(profile_result);
        } catch (err) {
            console.error('============================');
            console.error('Controller: Profile');
            console.error('Action: update');
            console.error('Error message: ', err.message);
            console.error('============================');
            return res.json(406, { message: err.message });
        }
    },
}