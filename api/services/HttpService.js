const SECRET = '';
const FORGET_SECRET = '';
module.exports = {
    requestToProfile: function(data, uripath, method){
		return new Promise((resolve, reject) => {
			let request = require('request');
			let jwt = require('jsonwebtoken');
			
			jwt.sign({ message: 'Now I\'m all messed up' }, SECRET, { expiresIn: '1h' }, function(err, token){
				if (err)
					reject({ message: err.message });
				else {
					request({
						url: ''+uripath,
						// url: ''+uripath,
						method: method,
						json: true,
						headers: { token: token },
						body: data
					}, (err, response, body) => {
						if (err || response.statusCode != 200) {
							let message = 'Something went wrong on request';
							if (body)
								message = body.message;
							reject({ message: message });
						} else
							resolve(body);
					})
				}
			});
		});
	},
    requestToForget: function(data, uripath, method){
		return new Promise((resolve, reject) => {
			let request = require('request');
			let jwt = require('jsonwebtoken');
			
			jwt.sign({ message: 'Now I\'m all messed up' }, FORGET_SECRET, { expiresIn: '1h' }, function(err, token){
				if (err)
					reject({ message: err.message });
				else {
					request({
						url: ''+uripath,
						// url: ''+uripath,
						method: method,
						json: true,
						headers: { token: token },
						body: data
					}, (err, response, body) => {
						if (err || response.statusCode != 200) {
							let message = 'Something went wrong on request';
							if (body)
								message = body.message;
							reject({ message: message });
						} else
							resolve(body);
					})
				}
			});
		});
	}
}