$(document).ready(function(){
    //navbar profile
    $('.nav .profile').on('click', function(){
        $('.nav .profile .desktop-nav').toggle();
    });
    $(document).resize(function(){
        if ($(window).width() < 768) {
            $('.nav .profile .desktop-nav').css('display', 'none');
        }
    });
});