$(document).ready(function(){
    //login
    $('#login_form').on('submit', function(e){
        e.preventDefault();
        var email = $('#login_form .email_form').val(),
        password = $('#login_form .password_form').val();
        $.ajax({
            url: '/login',
            method: 'post',
            data: { email: email, password: password },
            error: function(error){
                alert(error.responseJSON.message);
                return;
            },
            success: function(result){
                alert('Login successful');
                window.location.href = '/profile';
                return;
            }
        });
    });
    //register
    $('#register_form').on('submit', function(e){
        e.preventDefault();
        var firstname = $('#register_form .firstname_form').val(),
        lastname = $('#register_form .lastname_form').val(),
        email = $('#register_form .email_form').val(),
        password = $('#register_form .password_form').val();
        $.ajax({
            url: '/register',
            method: 'post',
            data: { firstname: firstname, lastname: lastname, email: email, password: password },
            error: function(error){
                alert(error.responseJSON.message);
            },
            success: function(result){
                alert('Register successful!');
                window.location.href = '/profile';
            }
        });
    });
    //forget
    $('#forget_form').on('submit', function(e){
        e.preventDefault();
        var email = $('#forget_form .email_form').val();
        $.ajax({
            url: '/forget-password',
            method: 'post',
            data: { email: email },
            error: function(error){
                alert('Please check your email inbox to reset new password');
            },
            success: function(result){
                alert('Please check your email inbox to reset new password');
                window.location.href = '/';
            }
        });
    });
    //reset
    $('#reset_form').on('submit', function(e){
        e.preventDefault();
        var email = $('#reset_form .email_form').val(),
        new_pwd = $('#reset_form .new_pwd_form').val(),
        confirm_pwd = $('#reset_form .confirm_pwd_form').val(),
        token = $('#reset_form .tkn_form').val();
        $.ajax({
            url: '/reset-password',
            method: 'post',
            data: { email: email, new_pwd: new_pwd, confirm_pwd: confirm_pwd, token: token },
            error: function(error){
                alert(error.message);
            },
            success: function(result){
                alert('Successful! Your password has been reset');
                window.location.href = '/';
            }
        });
    });
});