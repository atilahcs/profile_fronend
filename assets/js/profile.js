$(document).ready(function(){
    //update profile
    $('#profile_form').on('submit', function(e){
        e.preventDefault();
        var id, title = Number($('#profile_form input[name="title"]:checked').val()),
        email = $('#profile_form .email_form').val(),
        firstname = $('#profile_form .firstname_form').val(),
        lastname = $('#profile_form .lastname_form').val(),
        phone = $('#profile_form .phone_form').val()
        cookies = {};
        if (!title)
            title = 0;
        cookies = getCookie(document.cookie);
        id = Number(cookies.profile);
        $.ajax({
            url: '/profile',
            method: 'post',
            data: { id: id, title: title, email: email, firstname: firstname, lastname: lastname, phone: phone },
            error: function(error){
                alert(error.responseJSON.message);
            },
            success: function(result){
                alert('Update successful!');
                window.location.href = '/profile';
            }
        });
    });
    //update password
    $('#profile_pwd_form').on('submit', function(e){
        e.preventDefault();
        var id, 
        current_password = $('#profile_pwd_form .current_pwd_form').val(),
        new_password = $('#profile_pwd_form .new_pwd_form').val(),
        confirm_password = $('#profile_pwd_form .confirm_pwd_form').val()
        cookies = {};
        cookies = getCookie(document.cookie);
        id = Number(cookies.profile);
        $.ajax({
            url: '/profile/password',
            method: 'post',
            data: { id: id, current_password: current_password, new_password: new_password, confirm_password: confirm_password },
            error: function(error){
                alert(error.responseJSON.message);
            },
            success: function(result){
                alert('Update successful!');
                window.location.href = '/profile';
            }
        });
    });
});
function getCookie (cookie){
    var cookie_array = cookie.split('; '),
    cookies = {};
    for (var i=0;i<cookie_array.length;i++) {
        var attribute_name = cookie_array[i].substring(0, cookie_array[i].indexOf('='));
        var attribute_value = cookie_array[i].substring(cookie_array[i].indexOf('=')+1, cookie_array[i].length);
        cookies[attribute_name] = attribute_value;
    }
    return cookies;
}